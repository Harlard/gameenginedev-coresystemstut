#ifndef _GAMEOBJECTFACTORY_HPP_
#define _GAMEOBJECTFACTORY_HPP_

#include "GameObject.hpp"


class GameObjectFactory
{
    public:
	GameObjectFactory();
	~GameObjectFactory();

    static GameObject* create(const char* pType);

    static bool compareStrings(const char* str1, const char* str2);
};

#endif

